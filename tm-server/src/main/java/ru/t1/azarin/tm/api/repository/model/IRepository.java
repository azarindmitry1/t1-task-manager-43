package ru.t1.azarin.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.azarin.tm.model.AbstractModel;

public interface IRepository<M extends AbstractModel> {

    void add(@NotNull M model);

    void remove(@NotNull M model);

    void update(@NotNull M model);

}
